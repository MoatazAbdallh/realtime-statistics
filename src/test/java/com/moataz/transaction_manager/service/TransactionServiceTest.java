package com.moataz.transaction_manager.service;

import com.moataz.transaction_manager.api.v1.models.TransactionDto;
import com.moataz.transaction_manager.exceptions.ApiException;
import com.moataz.transaction_manager.services.StatisticsService;
import com.moataz.transaction_manager.services.TransactionService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {TransactionService.class, StatisticsService.class})
@TestPropertySource(properties = "app.scheduling.enable=false", locations = {"classpath:application.properties", "classpath:test.properties"})
public class TransactionServiceTest {
    @Autowired
    TransactionService transactionService;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testAddTransactionWithValidTimestamp() throws Exception {
        transactionService.createTransaction(new TransactionDto().amount(1.22).timestamp(OffsetDateTime.now(ZoneOffset.UTC).minusSeconds(10)));
        //Check the latest statistics updated
        Assert.assertEquals(Double.valueOf(1.22), TransactionService.getLatestStatics().getAvg());
        Assert.assertEquals(1, TransactionService.getLatestStatics().getCount());

        transactionService.createTransaction(new TransactionDto().amount(3.22).timestamp(OffsetDateTime.now(ZoneOffset.UTC).minusSeconds(40)));
        //Check the latest statistics updated
        Assert.assertEquals(Double.valueOf(2.22), TransactionService.getLatestStatics().getAvg());
        Assert.assertEquals(Double.valueOf(3.22), TransactionService.getLatestStatics().getMax());
        Assert.assertEquals(2, TransactionService.getLatestStatics().getCount());
    }

    @Test
    public void testAddTransactionWithFutureTimestamp() throws Exception {
        OffsetDateTime offsetDateTime = OffsetDateTime.now(ZoneOffset.UTC).plusSeconds(10);
        thrown.expect(ApiException.class);
        thrown.expectMessage(String.format("Future Transaction: %s", offsetDateTime));

        transactionService.createTransaction(new TransactionDto().amount(1.22).timestamp(offsetDateTime));
    }

    @Test
    public void testAddTransactionWithOutdatedTimestamp() throws Exception {
        OffsetDateTime offsetDateTime = OffsetDateTime.now(ZoneOffset.UTC).minusSeconds(70);
        thrown.expect(ApiException.class);
        thrown.expectMessage(String.format("Outdated Transaction: %s", offsetDateTime));

        transactionService.createTransaction(new TransactionDto().amount(1.22).timestamp(offsetDateTime));
    }
}

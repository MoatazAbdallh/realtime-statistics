package com.moataz.transaction_manager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moataz.transaction_manager.api.v1.models.TransactionDto;
import com.moataz.transaction_manager.services.TransactionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = "app.scheduling.enable=false", locations = {"classpath:application.properties", "classpath:test.properties"})
public class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransactionService transactionService;

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testAddTransaction() throws Exception {

        Mockito.doNothing().when(transactionService).createTransaction(ArgumentMatchers.any(TransactionDto.class));

        HashMap<String, String> transaction = new HashMap<>();
        transaction.put("amount", "1.22");
        transaction.put("timestamp", OffsetDateTime.now(ZoneOffset.UTC).minusSeconds(10).toString());
        mockMvc.perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(transaction)))
                .andExpect(status().isCreated());
    }

    @Test
    public void testAddInvalidTransaction() throws Exception {
        Mockito.doNothing().when(transactionService).createTransaction(ArgumentMatchers.any(TransactionDto.class));

        mockMvc.perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                .content("Invalid content"))
                .andExpect(status().isBadRequest());
    }

}

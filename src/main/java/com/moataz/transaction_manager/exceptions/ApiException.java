package com.moataz.transaction_manager.exceptions;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class ApiException extends RuntimeException {
    private HttpStatus errorCode;
    private String errorDetails;
    private LocalDateTime timestamp;

    public ApiException(HttpStatus errorCode) {
        this.timestamp = LocalDateTime.now();
        this.errorCode = errorCode;
    }


    public ApiException(HttpStatus errorCode, String errorMessage) {
        super(errorMessage);
        this.timestamp = LocalDateTime.now();
        this.errorCode = errorCode;
        this.errorDetails = "";
    }

    public ApiException(HttpStatus errorCode, String errorMessage, Throwable cause) {
        this(errorCode, errorMessage);
        super.initCause(cause);
    }

    public ApiException(HttpStatus errorCode, String errorMessage, String errorDetails, Throwable cause) {
        this(errorCode, errorMessage, cause);
        this.errorDetails = errorDetails;
    }

    public HttpStatus getErrorCode() {
        return errorCode;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
}

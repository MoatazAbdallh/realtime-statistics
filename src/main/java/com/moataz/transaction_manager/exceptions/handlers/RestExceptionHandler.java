package com.moataz.transaction_manager.exceptions.handlers;


import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.moataz.transaction_manager.api.v1.models.ApiErrorDto;
import com.moataz.transaction_manager.exceptions.ApiException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.io.PrintWriter;
import java.io.StringWriter;

@ControllerAdvice
public class RestExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ApiErrorDto> handleSystemException(ApiException ex, WebRequest request) {
        ModelMapper modelMapper = new ModelMapper();

        modelMapper.createTypeMap(ApiException.class, ApiErrorDto.class)
                .addMappings(mapper ->
                        mapper.map(ApiException::getMessage, ApiErrorDto::setErrorMessage)
                );
        LOGGER.error(ex.getMessage(), ex);
        return new ResponseEntity<>(modelMapper.map(ex, ApiErrorDto.class), ex.getErrorCode());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ApiErrorDto> handleInvalidRequest(HttpMessageNotReadableException ex, WebRequest request) {
        LOGGER.error(ex.getMessage(), ex);
        ApiErrorDto apiErrorDto = new ApiErrorDto();
        if (ex.getCause() instanceof InvalidFormatException) {
            apiErrorDto.setErrorMessage("Deserialize issue " + ex.getMessage());
            return new ResponseEntity<>(apiErrorDto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        apiErrorDto.setErrorMessage("Malformed request body" + ex.getMessage());
        return new ResponseEntity<>(apiErrorDto, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiErrorDto> handleInternalServerError(Exception ex, WebRequest request) {
        LOGGER.error(ex.getMessage(), ex);
        ApiErrorDto apiErrorDto = new ApiErrorDto();
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        ex.printStackTrace(printWriter);
        apiErrorDto.setErrorMessage("Internal Server Error " + (ex.getMessage() == null ? stringWriter.toString() : ex.getMessage()));
        return new ResponseEntity<>(apiErrorDto, HttpStatus.BAD_REQUEST);
    }
}

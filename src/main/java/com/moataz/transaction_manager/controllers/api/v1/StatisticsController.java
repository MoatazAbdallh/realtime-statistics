package com.moataz.transaction_manager.controllers.api.v1;

import com.moataz.transaction_manager.api.v1.StatisticsApi;
import com.moataz.transaction_manager.api.v1.models.StatisticsDto;
import com.moataz.transaction_manager.services.TransactionService;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;

@RestController
@RequestMapping("/")
public class StatisticsController implements StatisticsApi {

    @Override
    public ResponseEntity<StatisticsDto> getStatistics() {
        ModelMapper modelMapper = new ModelMapper();

        Converter<Double, String> toBigDecimal = new AbstractConverter<Double, String>() {
            protected String convert(Double source) {
                return source == null ? "0.00" : new BigDecimal(source).setScale(2, RoundingMode.HALF_UP).toString();
            }
        };
        modelMapper.addConverter(toBigDecimal);
        StatisticsDto statistics = modelMapper.map(TransactionService.getLatestStatics(), StatisticsDto.class);
        return new ResponseEntity<>(statistics, HttpStatus.OK);

    }
}

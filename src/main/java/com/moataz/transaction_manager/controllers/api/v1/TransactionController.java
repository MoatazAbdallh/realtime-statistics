package com.moataz.transaction_manager.controllers.api.v1;

import com.moataz.transaction_manager.api.v1.TransactionsApi;
import com.moataz.transaction_manager.api.v1.models.TransactionDto;
import com.moataz.transaction_manager.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class TransactionController implements TransactionsApi {

    @Autowired
    TransactionService transactionService;

    @Override
    public ResponseEntity<Void> createTransaction(TransactionDto transaction) {
        transactionService.createTransaction(transaction);

        return new ResponseEntity<>(HttpStatus.CREATED);

    }

    @Override
    public ResponseEntity<Void> deleteTransactions() {
        transactionService.deleteTransactions();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}


package com.moataz.transaction_manager.services;

import com.moataz.transaction_manager.entities.Statistics;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class StatisticsService {

    /**
     * Update Statistics whenever we have new transaction meets the requirement, which it's timestamp - currentTime < 60 sec
     *
     * @param amount     transaction amount
     * @param statistics statistics needs to be updated
     */
    public void updateStatistics(Statistics statistics, Double amount) {
        statistics.setCount(statistics.getCount() + 1);
        if (statistics.getCount() == 1) {
            statistics.setSum(amount);
            statistics.setAvg(amount);
            statistics.setMin(amount);
            statistics.setMax(amount);
            return;
        }
        BigDecimal sum = BigDecimal.valueOf(statistics.getSum()).add(BigDecimal.valueOf(amount)).setScale(2,
                RoundingMode.HALF_UP);
        statistics.setSum(sum.doubleValue());
        statistics.setAvg(sum.divide(BigDecimal.valueOf(statistics.getCount()), RoundingMode.HALF_UP).doubleValue());
        statistics.setMin(Math.min(statistics.getMin(), amount));
        statistics.setMax(Math.max(statistics.getMax(), amount));

    }

    public void resetStatistics(Statistics statistics) {
        statistics.setSum(0d);
        statistics.setAvg(0d);
        statistics.setMin(0d);
        statistics.setMax(0d);
        statistics.setCount(0);
    }

    /**
     * removes expired statistics from latest statistics
     *
     * @param originalStatistics  object to remove
     * @param removableStatistics removing object
     * @param minMaxAmount        min and max amount values of all seconds
     */
    public void remove(Statistics originalStatistics, Statistics removableStatistics, Double[] minMaxAmount) {
        //subtract the removable sum from the latest sum
        BigDecimal newSum = BigDecimal.valueOf(originalStatistics.getSum()).subtract(BigDecimal.valueOf(removableStatistics.getSum()))
                .setScale(2, RoundingMode.HALF_UP);
        originalStatistics.setSum(newSum.doubleValue());
        //subtract count
        long newCount = originalStatistics.getCount() - removableStatistics.getCount();
        originalStatistics.setCount(newCount);

        if (originalStatistics.getCount() > 0) {
            originalStatistics.setAvg(
                    newSum.divide(BigDecimal.valueOf(newCount), 2, RoundingMode.HALF_UP)
                            .doubleValue());
            originalStatistics.setMin(minMaxAmount[0]);
            originalStatistics.setMax(minMaxAmount[1]);
            return;
        }

        originalStatistics.setAvg(0d);
        originalStatistics.setMin(0d);
        originalStatistics.setMax(0d);

    }
}

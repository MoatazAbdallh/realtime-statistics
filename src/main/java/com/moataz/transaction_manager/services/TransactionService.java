package com.moataz.transaction_manager.services;

import com.moataz.transaction_manager.api.v1.models.TransactionDto;
import com.moataz.transaction_manager.entities.Statistics;
import com.moataz.transaction_manager.entities.Transaction;
import com.moataz.transaction_manager.exceptions.ApiException;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

@Service
@ConditionalOnProperty(prefix = "app.scheduling.enable", name="enabled", havingValue="true", matchIfMissing = true)
public class TransactionService {

    @Value("${transaction.expiring.duration}")
    private long transactionExpiredDuration;

    @Autowired
    private StatisticsService statisticsService;

    private static final Logger logger = LoggerFactory.getLogger(TransactionService.class);

    private static final Map<Long, Statistics> statisticsMap = new ConcurrentHashMap<>();

    private static Statistics latestStatics = new Statistics();

    //For fairness locks favor granting access to the longest-waiting thread, which may be helpful in retrieving the latestStatics statistics
    private ReentrantLock lock = new ReentrantLock(true);

    /**
     * Calculate difference between current time and transaction timestamp to reject those with time stamp older than the current time
     * & also if the difference between the current time and transaction timestamp is greater than transaction expiring period
     *
     * @param transactionDto transaction data transfer object that received through transaction controller
     */
    public void createTransaction(TransactionDto transactionDto) {
        long epochTime = Instant.now(Clock.systemUTC()).getEpochSecond();
        //DTO to Model Mapper
        ModelMapper modelMapper = new ModelMapper();
        Converter<OffsetDateTime, Long> toEpochSecond = new AbstractConverter<OffsetDateTime, Long>() {
            protected Long convert(OffsetDateTime source) {
                return source == null ? null : source.toEpochSecond();
            }
        };
        modelMapper.addConverter(toEpochSecond);
        Transaction transaction = modelMapper.map(transactionDto, Transaction.class);
        long diff = epochTime - transaction.getTimestamp();

        if (epochTime < transaction.getTimestamp()) {
            logger.debug("Future Transaction: {}", transactionDto.getTimestamp());
            throw new ApiException(HttpStatus.UNPROCESSABLE_ENTITY, String.format("Future Transaction: %s", transactionDto.getTimestamp()));

        }

        if (diff >= transactionExpiredDuration) {
            // Older transactions reject 'em
            logger.debug("Outdated Transaction: {}", transactionDto.getTimestamp());
            throw new ApiException(HttpStatus.NO_CONTENT, String.format("Outdated Transaction: %s", transactionDto.getTimestamp()));
        }

        addTransactionAndUpdateLatestStatistics(transaction);
    }

    /**
     * lock over the latestStatics statistics to be thread safe during update, add add the new statistics in the concurrentMap
     *
     * @param transaction Mapped Transaction
     */
    private void addTransactionAndUpdateLatestStatistics(Transaction transaction) {
        lock.lock();
        try {
            statisticsService.updateStatistics(latestStatics, transaction.getAmount());
        } finally {
            lock.unlock();
        }

        if (statisticsMap.containsKey(transaction.getTimestamp())) {
            statisticsService.updateStatistics(statisticsMap.get(transaction.getTimestamp()), transaction.getAmount());
            return;
        }
        Statistics statistics = new Statistics();
        statisticsService.updateStatistics(statistics, transaction.getAmount());
        statisticsMap.put(transaction.getTimestamp(), statistics);
    }

    /**
     * Reset the latestStatistics and empty the concurrentMap
     */
    public void deleteTransactions() {
        lock.lock();
        try {
            statisticsService.resetStatistics(latestStatics);
        } finally {
            lock.unlock();
        }
        statisticsMap.clear();
    }

    public static Statistics getLatestStatics() {
        return latestStatics;
    }

    /**
     * Execute Cron job to remove the outdated transactions
     */
    @Scheduled(cron = "${timing.remove.old.records}")
    public void removeOldRecords() {
        System.out.println("Remove");
        long currentEpoch = Instant.now(Clock.systemUTC()).getEpochSecond();

        long outdatedTimestamp = currentEpoch - transactionExpiredDuration;
        Statistics statistics = statisticsMap.remove(outdatedTimestamp);
        if (statistics == null) {
            return;
        }
        Double[] minMaxAmount = new Double[]{0.00, 0.00};
        statisticsMap.entrySet()
                .parallelStream()
                .min(Comparator.comparingDouble(entry -> entry.getValue().getMin())).ifPresent(newMinStatistics -> {
            minMaxAmount[0] = newMinStatistics.getValue().getMin();
        });

        statisticsMap.entrySet()
                .parallelStream()
                .max(Comparator.comparingDouble(entry -> entry.getValue().getMax())).ifPresent(newMinStatistics -> {
            minMaxAmount[1] = newMinStatistics.getValue().getMax();
        });

        lock.lock();
        try {
            statisticsService.remove(latestStatics, statistics, minMaxAmount);
        } finally {
            lock.unlock();
        }
    }
}
